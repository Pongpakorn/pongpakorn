﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;

public class HealthPickUp : MonoBehaviour
{
    PlayerSpaceship playerHealth;

    void Awake()
    {
        playerHealth = FindObjectOfType<PlayerSpaceship>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (playerHealth.Hp < 100)
        {
            Destroy(gameObject);
        }
    }
}
