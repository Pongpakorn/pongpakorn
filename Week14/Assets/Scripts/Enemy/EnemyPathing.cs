﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPathing : MonoBehaviour
{
    [SerializeField] private List<Transform> waypoints;
    [SerializeField] private float movespeed = 2f;
    private int waypointIndex = 0;
    
    void Start()
    {
        transform.position = waypoints[waypointIndex].transform.position;
        
    }

    private void Update()
    {
        move();
    }

    // Update is called once per frame
    private void move()
    {
        if (waypointIndex <= waypoints.Count - 1)
        {
            var targetPosition = waypoints[waypointIndex].transform.position;
            var movementThisFrame = movespeed * Time.deltaTime;
            transform.position = Vector2.MoveTowards(transform.position, targetPosition, movementThisFrame);

            if (transform.position == targetPosition)
            {
                waypointIndex++;
            }
            
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
